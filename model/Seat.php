<?php

class Seat implements JsonSerializable {
    
    private $id;
    private $row;
    private $column;
    private $price;
    private $isAvaliable;

    public function __construct(int $id, string $row, int $column, bool $isAvaliable = true) {
        $this->id = $id;
        $this->row = $row;
        $this->column = $column;
        $this->isAvaliable = $isAvaliable;
    }

    public function getId(): int {
        return $this->id;
    }

    public function getRow(): string {
        return $this->row;
    }

    public function getColumn(): int {
        return $this->column;
    }

    public function getPrice(): int {
        return $this->price;
    }

    public function getIsAvaliable(): bool {
        return $this->isAvaliable;
    }

    public function setPrice(int $price) {
        return $this->price = $price;
    }

    public function setIsAvaliable(bool $isAvaliable) {
        return $this->isAvaliable = $isAvaliable;
    }

    public function jsonSerialize() {
       return (object) get_object_vars($this);
    }
}

