<?php

require_once('model/Theater.php');

class Room implements JsonSerializable {
    
    private $id;
    private $name;
    private $theater;
    private $sections;

    public function __construct(int $id, string $name, Theater $theater, array $sections) {
        $this->id = $id;
        $this->name = $name;
        $this->theater = $theater;
        $this->sections = $sections;
    }

    public function getId(): int {
        return $this->id;
    }

    public function getName(): string {
        return $this->name;
    }

    public function getSections(): array {
        return $this->sections;
    }

    public function getTheater(): Theater {
        return $this->theater;
    }

    public function jsonSerialize() {
       return (object) get_object_vars($this);
    }
}