<?php

class Play implements JsonSerializable {
    
    private $id;
    private $name;
    private $performances;

    public function __construct(int $id, string $name, array $performances = null) {
        $this->id = $id;
        $this->name = $name;
        $this->performances = $performances ?? [];
    }

    public function getId(): int {
        return $this->id;
    }

    public function getName(): string {
        return $this->name;
    }

    public function getPerformances(): array {
        return $this->performances;
    }

    public function setPerformances(array $performances): array {
        return $this->performances = $performances;
    }

    public function jsonSerialize() {
       return (object) get_object_vars($this);
    }
}

