<?php

class Performance implements JsonSerializable {
    
    private $id;
    private $date;
    private $room;

    public function __construct(int $id, DateTime $date, Room $room) {
        $this->id = $id;
        $this->date = $date;
        $this->room = $room;
    }

    public function getId(): int {
        return $this->id;
    }

    public function getdate(): DateTime {
        return $this->date;
    }

    public function getRoom(): Room {
        return $this->room;
    }

    public function jsonSerialize() {
       return (object) get_object_vars($this);
    }
}