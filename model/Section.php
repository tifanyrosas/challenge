<?php

class Section implements JsonSerializable {
    
    private $id;
    private $name;
    private $seats;

    public function __construct(int $id, string $name, array $seats) {
        $this->id = $id;
        $this->name = $name;
        $this->seats = $seats;
    }

    public function getId(): int {
        return $this->id;
    }

    public function getName(): string {
        return $this->name;
    }

    public function getSeats(): array {
        return $this->seats;
    }

    public function jsonSerialize() {
       return (object) get_object_vars($this);
    }
}