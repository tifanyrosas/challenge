-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 10-12-2020 a las 00:43:09
-- Versión del servidor: 10.2.10-MariaDB
-- Versión de PHP: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `playsportal`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `performance`
--

CREATE TABLE `performance` (
  `id` int(11) NOT NULL,
  `playId` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `roomId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `performance`
--

INSERT INTO `performance` (`id`, `playId`, `date`, `roomId`) VALUES
(1, 1, '2020-12-29 21:01:10', 1),
(2, 1, '2020-12-18 20:00:00', 1),
(4, 2, '2020-12-30 19:00:00', 2),
(5, 3, '2020-11-29 19:00:00', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `performanceSeat`
--

CREATE TABLE `performanceSeat` (
  `performanceId` int(11) NOT NULL,
  `seatId` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `avaliable` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `performanceSeat`
--

INSERT INTO `performanceSeat` (`performanceId`, `seatId`, `price`, `avaliable`) VALUES
(1, 1, 1000, 1),
(1, 2, 1000, 1),
(1, 4, 800, 1),
(1, 5, 800, 1),
(2, 1, 1000, 1),
(2, 2, 1000, 1),
(2, 4, 800, 1),
(2, 5, 800, 1),
(4, 6, 1500, 1),
(4, 7, 1500, 1),
(5, 9, 1200, 1),
(5, 10, 1200, 1),
(5, 11, 900, 1),
(5, 12, 900, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `play`
--

CREATE TABLE `play` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `play`
--

INSERT INTO `play` (`id`, `name`) VALUES
(1, 'The Lion King'),
(2, 'Aladdin'),
(3, 'Chicago'),
(4, 'Wicked');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reservation`
--

CREATE TABLE `reservation` (
  `id` int(11) NOT NULL,
  `dni` varchar(30) NOT NULL,
  `personName` varchar(250) NOT NULL,
  `performanceId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reservationSeat`
--

CREATE TABLE `reservationSeat` (
  `reservationId` int(11) NOT NULL,
  `performanceId` int(11) NOT NULL,
  `seatId` int(11) NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `room`
--

CREATE TABLE `room` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `theaterId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `room`
--

INSERT INTO `room` (`id`, `name`, `theaterId`) VALUES
(1, 'RoomA', 1),
(2, 'RoomB', 1),
(3, 'RoomC', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `seat`
--

CREATE TABLE `seat` (
  `id` int(11) NOT NULL,
  `row` varchar(10) NOT NULL,
  `column` int(11) NOT NULL,
  `sectionId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `seat`
--

INSERT INTO `seat` (`id`, `row`, `column`, `sectionId`) VALUES
(1, 'A', 1, 1),
(2, 'A', 2, 1),
(4, 'B', 1, 2),
(5, 'B', 2, 2),
(6, 'A', 11, 3),
(7, 'A', 12, 3),
(9, 'D', 1, 4),
(10, 'D', 2, 4),
(11, 'F', 1, 5),
(12, 'F', 2, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `section`
--

CREATE TABLE `section` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `roomId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `section`
--

INSERT INTO `section` (`id`, `name`, `roomId`) VALUES
(1, 'Sec 1', 1),
(2, 'Sec 2', 1),
(3, 'Sec 1', 2),
(4, 'Sec 1', 3),
(5, 'Sec 2', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `theater`
--

CREATE TABLE `theater` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `theater`
--

INSERT INTO `theater` (`id`, `name`) VALUES
(1, 'National Theater'),
(2, 'Lyric Theater');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `performance`
--
ALTER TABLE `performance`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_performance_playId` (`playId`),
  ADD KEY `fk_performance_roomId` (`roomId`);

--
-- Indices de la tabla `performanceSeat`
--
ALTER TABLE `performanceSeat`
  ADD PRIMARY KEY (`performanceId`,`seatId`),
  ADD KEY `fk_performanceSeat_seatId` (`seatId`);

--
-- Indices de la tabla `play`
--
ALTER TABLE `play`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `reservation`
--
ALTER TABLE `reservation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_reservation_performanceId` (`performanceId`);

--
-- Indices de la tabla `reservationSeat`
--
ALTER TABLE `reservationSeat`
  ADD PRIMARY KEY (`reservationId`,`seatId`,`performanceId`) USING BTREE,
  ADD UNIQUE KEY `performance_seat` (`performanceId`,`seatId`),
  ADD KEY `fk_reservationSeat_seatId` (`seatId`,`performanceId`) USING BTREE;

--
-- Indices de la tabla `room`
--
ALTER TABLE `room`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_room_theaterId` (`theaterId`);

--
-- Indices de la tabla `seat`
--
ALTER TABLE `seat`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_seat_sectionId` (`sectionId`);

--
-- Indices de la tabla `section`
--
ALTER TABLE `section`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_section_roomId` (`roomId`);

--
-- Indices de la tabla `theater`
--
ALTER TABLE `theater`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `performance`
--
ALTER TABLE `performance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `play`
--
ALTER TABLE `play`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `reservation`
--
ALTER TABLE `reservation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `room`
--
ALTER TABLE `room`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `seat`
--
ALTER TABLE `seat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `section`
--
ALTER TABLE `section`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `theater`
--
ALTER TABLE `theater`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `performance`
--
ALTER TABLE `performance`
  ADD CONSTRAINT `fk_performance_playId` FOREIGN KEY (`playId`) REFERENCES `play` (`id`),
  ADD CONSTRAINT `fk_performance_roomId` FOREIGN KEY (`roomId`) REFERENCES `room` (`id`);

--
-- Filtros para la tabla `performanceSeat`
--
ALTER TABLE `performanceSeat`
  ADD CONSTRAINT `fk_performanceSeat_reservationId` FOREIGN KEY (`performanceId`) REFERENCES `performance` (`id`),
  ADD CONSTRAINT `fk_performanceSeat_seatId` FOREIGN KEY (`seatId`) REFERENCES `seat` (`id`);

--
-- Filtros para la tabla `reservation`
--
ALTER TABLE `reservation`
  ADD CONSTRAINT `fk_reservation_performanceId` FOREIGN KEY (`performanceId`) REFERENCES `performance` (`id`);

--
-- Filtros para la tabla `reservationSeat`
--
ALTER TABLE `reservationSeat`
  ADD CONSTRAINT `fk_reservationSeat_performanceId` FOREIGN KEY (`performanceId`) REFERENCES `performance` (`id`),
  ADD CONSTRAINT `fk_reservationSeat_reservationId` FOREIGN KEY (`reservationId`) REFERENCES `reservation` (`id`),
  ADD CONSTRAINT `fk_reservationSeat_reservationeId` FOREIGN KEY (`reservationId`) REFERENCES `reservation` (`id`),
  ADD CONSTRAINT `fk_reservationSeat_seatId` FOREIGN KEY (`seatId`) REFERENCES `seat` (`id`);

--
-- Filtros para la tabla `room`
--
ALTER TABLE `room`
  ADD CONSTRAINT `fk_room_theaterId` FOREIGN KEY (`theaterId`) REFERENCES `theater` (`id`);

--
-- Filtros para la tabla `seat`
--
ALTER TABLE `seat`
  ADD CONSTRAINT `fk_seat_sectionId` FOREIGN KEY (`sectionId`) REFERENCES `section` (`id`);

--
-- Filtros para la tabla `section`
--
ALTER TABLE `section`
  ADD CONSTRAINT `fk_section_roomId` FOREIGN KEY (`roomId`) REFERENCES `room` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;