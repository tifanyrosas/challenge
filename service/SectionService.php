<?php

include_once('dao/SectionDao.php');
include_once('model/Section.php');

class SectionService {
	
	private $dao;

	public function __construct($dbConnection) {
		$this->dao = new SectionDao($dbConnection);
	}

	function getSectionsByRoomId(int $roomId): array {
		return $this->dao->getSectionsByRoomId($roomId);
	}
}
