<?php

include_once('dao/PlayDao.php');
include_once('PerformanceService.php');
require_once('PlaySearchCriteria.php');


class PlayService {
	
	private $dao;
	private $db;

	public function __construct($dbConnection) {
		$this->dao = new PlayDao($dbConnection);
		$this->db = $dbConnection;
	}

	function getPlays(PlaySearchCriteria $searchCriteria): array {
		return $this->dao->getPlays($searchCriteria);
	}

	function getAvaliableSeats(int $playId, int $performanceId) {
		$play = $this->dao->getById($playId);
		$filteredPerformance = array_values(
			array_filter($play->getPerformances(), function($performance) use ($performanceId) {
				return $performance->getId() == $performanceId;
			})
		);
		$performance = count($filteredPerformance) > 0 ? $filteredPerformance[0] : null;
		if(!$performance) {
			throw new Exception('Bad request');
		} else {
			$performanceService = new PerformanceService($this->db);
			return $performanceService->getAvaliableSeats($performanceId);
		}
	}
}
