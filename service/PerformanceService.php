<?php

include_once('dao/PerformanceDao.php');
include_once('model/Performance.php');

class PerformanceService {
	
	private $dao;

	public function __construct($dbConnection) {
		$this->dao = new PerformanceDao($dbConnection);
	}

	function getById(int $performanceId) {
		return $this->dao->getById($performanceId);
	}

	function getPerformancesByPlayId(int $playId, PlaySearchCriteria $searchCriteria = null): array {
		return $this->dao->getPerformancesByPlayId($playId, $searchCriteria);
	}

	function getAvaliableSeats(int $performanceId) {
		return $this->dao->getAvaliableSeats($performanceId);
	}

	function getSeatPrice(int $performanceId, int $seatId) {
		return $this->dao->getSeatPrice($performanceId, $seatId);
	}

	function setSeatAsUnavaliable(int $performanceId, int $seatId) {
		return $this->dao->setSeatAsUnavaliable($performanceId, $seatId);
	}
}
