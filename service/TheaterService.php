<?php

include_once('dao/TheaterDao.php');
include_once('model/Theater.php');

class TheaterService {
	
	private $dao;

	public function __construct($dbConnection) {
		$this->dao = new TheaterDao($dbConnection);
	}

	function getById(int $theaterId): Theater {
		return $this->dao->getById($theaterId);
	}
}
