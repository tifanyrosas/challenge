<?php

include_once('dao/RoomDao.php');
include_once('model/Room.php');

class RoomService {
	
	private $dao;

	public function __construct($dbConnection) {
		$this->dao = new RoomDao($dbConnection);
	}

	function getById(int $roomId) {
		return $this->dao->getById($roomId);
	}
}
