<?php

include_once('dao/ReservationDao.php');

class ReservationService {
	
	private $dao;

	public function __construct($dbConnection) {
		$this->dao = new ReservationDao($dbConnection);
	}

	function makeReservation(int $performanceId, array $seatsId, string $dni, string $personName) {
		return $this->dao->makeReservation($performanceId, $seatsId, $dni, $personName);
	}
}
