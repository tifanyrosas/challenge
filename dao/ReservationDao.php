<?php

require_once('model/Performance.php');
require_once('model/Seat.php');


class ReservationDao {

    private $db;
    private $performanceService;

    function __construct(PDO $dbConnection) {
        $this->db = $dbConnection;
        $this->performanceService = new PerformanceService($this->db);
    }

    function makeReservation(int $performanceId, array $seatsId, string $dni, string $personName) {
        try {
            if ($this->validateSeats($performanceId, $seatsId)) {
                $this->db->beginTransaction();
                $reservationId = $this->saveReservation($performanceId, $dni, $personName);
                $this->saveReservationSeats($reservationId, $performanceId, $seatsId);
                $this->db->commit();
                return $reservationId;
            }
        } catch (\PDOException $e) {
            $this->db->rollback();
        }
    }

    private function validateSeats(int $performanceId, array $seatsId): bool {
        $avaliableSeats = $this->performanceService->getAvaliableSeats($performanceId);
        $avaliableSeatsId = array_map(function (Seat $seat) {
            return $seat->getId();
		}, $avaliableSeats);
        $seatsDontBelongToPerformance = array_diff($seatsId, $avaliableSeatsId);
        return count($seatsDontBelongToPerformance) == 0;
    }

    private function saveReservation(int $performanceId, string $dni, string $personName): int {
        $statement = $this->db->prepare("INSERT INTO reservation (dni, personName, performanceId)
			VALUES (:dni, :personName, :performanceId)");
        $statement->execute(array(
            'dni' => $dni,
            'personName' => $personName,
            'performanceId' => $performanceId
        ));
        return $this->db->lastInsertId();
    }

    private function saveReservationSeats(int $reservationId, int $performanceId, array $seatsId) {
        $reservationSeatStatement = $this->db->prepare("INSERT INTO reservationSeat (reservationId, performanceId, seatId, price)
			VALUES (:reservationId, :performanceId, :seatId, :price)");

        foreach ($seatsId as $seatId) {
            $seatPrice = $this->performanceService->getSeatPrice($performanceId, $seatId);
            $result = $reservationSeatStatement->execute(array(
                'reservationId' => $reservationId,
                'performanceId' => $performanceId,
                'seatId' => $seatId,
                'price' => $seatPrice
            ));
            if (!$result) {
                throw new PDOException("Error Processing Request");
            }
            $this->performanceService->setSeatAsUnavaliable($performanceId, $seatId);
        }
    }
}
