<?php

require_once('model/Play.php');
require_once('model/Performance.php');
require_once('service/PerformanceService.php');
require_once('PlaySearchCriteria.php');


class PlayDao {

	private $db;
    private $memcache;

	function __construct($dbConnection) {
		$this->db = $dbConnection;
        $this->performanceService = new PerformanceService($this->db);
	}
	
	function getPlays(PlaySearchCriteria $searchCriteria): array {
        $query = "SELECT play.id, play.name FROM play";
		$statement = $this->db->prepare($query);
        try {
            $statement->execute();
            $plays = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $playsObject = [];
            foreach ($plays as $play) {
            	$playsObject[] = $this->buildPlay($play, $searchCriteria);
            }
            return $playsObject;
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
	}

	function getById(int $id): Play {
		$statement = $this->db->prepare("SELECT play.id, play.name FROM play WHERE id = " . $id);
		try {
            $statement->execute();
            $play = $statement->fetch(\PDO::FETCH_ASSOC);
            $onePlay = new Play($play['id'], $play['name']);
            $onePlay->setPerformances($this->performanceService->getPerformancesByPlayId($onePlay->getId()));
            return $onePlay;
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
	}

    private function buildPlay(array $playData, PlaySearchCriteria $searchCriteria): Play {
        $onePlay = new Play($playData['id'], $playData['name']);
        $onePlay->setPerformances($this->performanceService->getPerformancesByPlayId($onePlay->getId(), $searchCriteria));
        return $onePlay;
    }
}
