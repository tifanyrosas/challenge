<?php

require_once('model/Play.php');
require_once('model/Performance.php');
require_once('model/Seat.php');
require_once('service/RoomService.php');


class PerformanceDao {

	private $db;
	private $roomService;

	public function __construct($dbConnection) {
		$this->db = $dbConnection;
		$this->roomService = new RoomService($this->db);
	}

	function getById(int $performanceId): Performance {
		$statement = $this->db->prepare("SELECT p.id, p.date, p.roomId 
			FROM performance p
			WHERE id = " . $performanceId);
		try {
            $statement->execute();
            $performanceData = $statement->fetch(\PDO::FETCH_ASSOC);
            $room = $this->roomService->getById($performanceData['roomId']);
            foreach ($room->getSections() as $section) {
            	$seats = $section->getSeats();
            	foreach ($seats as &$seat) {
            		$seat->setPrice($this->getSeatPrice($performanceId, $seat->getId()));
            		$seat->setIsAvaliable($this->isSeatAvaliable($performanceId, $seat->getId()));
            	}
            }
           
            $performance = new Performance($performanceData['id'], new DateTime($performanceData['date']), $room);
            return $performance;
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
	}

	function getPerformancesByPlayId(int $playId, PlaySearchCriteria $searchCriteria = null): array {
		$statement = $this->db->prepare($this->buildGetPerformancesQuery($playId, $searchCriteria));
		try {
            $statement->execute();
            $performances = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $performancesObject = [];
            foreach ($performances as $performance) {
            	$performancesObject[] = $this->getById($performance['id']);
            }
            return $performancesObject;
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
	}

	
	function getAvaliableSeats(int $performanceId): array {
		$statement = $this->db->prepare("SELECT s.id, s.row, s.column, ps.price 
			FROM performanceSeat ps JOIN seat s ON ps.seatId = s.id 
			WHERE performanceId = " . $performanceId . " AND avaliable = 1");
		try {
            $statement->execute();
            $avaliableSeats = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $seats = [];
            foreach ($avaliableSeats as $avaliableSeat) {
            	$aSeat = new Seat($avaliableSeat['id'], $avaliableSeat['row'], $avaliableSeat['column']);
            	$aSeat->setPrice($avaliableSeat['price']);
            	$seats[] = $aSeat;
            }
            return $seats;
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
	}

	function getSeatPrice(int $performanceId, int $seatId): int {
		$statement = $this->db->prepare("SELECT ps.price 
			FROM performanceSeat ps
			WHERE performanceId = " . $performanceId . " AND seatId = " . $seatId);
		try {
            $statement->execute();
            $price = $statement->fetch(\PDO::FETCH_ASSOC);
            return $price ? (int) $price['price'] : 0;
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
	}

	function isSeatAvaliable(int $performanceId, int $seatId): bool {
		$statement = $this->db->prepare("SELECT ps.avaliable 
			FROM performanceSeat ps
			WHERE performanceId = " . $performanceId . " AND seatId = " . $seatId);
		try {
            $statement->execute();
            $avaliable = $statement->fetch(\PDO::FETCH_ASSOC);
            return $avaliable ? (bool) $avaliable['avaliable'] : false;
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
	}

	function setSeatAsUnavaliable(int $performanceId, int $seatId) {
		$statement = $this->db->prepare("UPDATE performanceSeat SET avaliable = 0 WHERE seatId = " . $seatId . " AND performanceId = " . $performanceId);
        $statement->execute();
	}

	private function buildGetPerformancesQuery(int $playId, PlaySearchCriteria $searchCriteria = null): string {
		$sql = "SELECT p.id
			FROM performance p";
		$whereCondition = " WHERE playId = ".$playId;
		if ($searchCriteria) {
            $sql .= $this->getOrderStatment($whereCondition, $searchCriteria);
        } else {
            $sql .= $whereCondition;
        }
		return $sql;
	}

	private function getOrderStatment(string $whereCondition, PlaySearchCriteria $searchCriteria): string {
        switch ($searchCriteria->getOrderCriteria()) {
            case PlaySearchCriteria::ORDER_CRITERIA_DATE: 
                $searchStatment = $whereCondition ." ORDER BY p.date ".$searchCriteria->getOrder();
                break;
            case PlaySearchCriteria::ORDER_CRITERIA_PRICE:
                $searchStatment = " JOIN performanceSeat ps ON p.id = ps.performanceId ".$whereCondition." GROUP BY p.id ORDER BY ps.price ".$searchCriteria->getOrder();
                break;
            default:
                $searchStatment = '';
        }
        return $searchStatment;
    }
}
