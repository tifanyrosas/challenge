<?php

require_once('model/Theater.php');

class TheaterDao {

	private $db;

	public function __construct($dbConnection) {
		$this->db = $dbConnection;
	}

	function getById(int $theaterId): Theater {
		$statement = $this->db->prepare("SELECT id, name
			FROM theater 
			WHERE id = " . $theaterId);
		try {
            $statement->execute();
            $theaterData = $statement->fetch(\PDO::FETCH_ASSOC);
            return new Theater($theaterData['id'], $theaterData['name']);
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
	}
}
