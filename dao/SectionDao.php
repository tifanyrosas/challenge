<?php

require_once('model/Section.php');

class SectionDao {

	private $db;
	private $seatService;

	public function __construct($dbConnection) {
		$this->db = $dbConnection;
	}

	function getSectionsByRoomId(int $roomId): array {
		$statement = $this->db->prepare("SELECT s.id, s.name
			FROM section s
			WHERE roomId = " . $roomId);
		try {
            $statement->execute();
            $sectionsData = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $sections = [];
            foreach ($sectionsData as $sectionData) {
            	$seats = $this->getSeatsBySectionId($sectionData['id']);
            	$sections[] = new Section($sectionData['id'], $sectionData['name'], $seats);
            }
            return $sections;
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
	}

	function getSeatsBySectionId(int $sectionId): array {
		$statement = $this->db->prepare("SELECT s.id, s.row, s.column
			FROM seat s
			WHERE s.sectionId = " . $sectionId);
		try {
            $statement->execute();
            $seatsData = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $seats = [];
            foreach ($seatsData as $seatData) {
            	$seat = new Seat($seatData['id'], $seatData['row'], $seatData['column']);
                $seats[] = $seat;
            }
            return $seats;
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
	}
}
