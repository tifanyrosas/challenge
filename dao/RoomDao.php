<?php

require_once('model/Room.php');
require_once('service/SectionService.php');
require_once('service/TheaterService.php');

class RoomDao {

	private $db;
	private $sectionService;
	private $theaterService;

	public function __construct($dbConnection) {
		$this->db = $dbConnection;
		$this->sectionService = new SectionService($this->db);
		$this->theaterService = new TheaterService($this->db);
	}

	function getById(int $roomId): Room {
		$statement = $this->db->prepare("SELECT r.id, r.name, r.theaterId
			FROM room r
			WHERE id = " . $roomId);
		try {
            $statement->execute();
            $roomData = $statement->fetch(\PDO::FETCH_ASSOC);
            $sections = $this->sectionService->getSectionsByRoomId($roomData['id']);
            $theater = $this->theaterService->getById($roomData['theaterId']);
            return new Room($roomData['id'], $roomData['name'], $theater, $sections);
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
	}
}
