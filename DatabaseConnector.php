<?php

class DatabaseConnector {

    private $dbConnection = null;

    public function __construct() {
        $host = 'localhost';
        $db   = 'playsportal';
        $user = 'root';
        $pass = 'asd123';
        $port = '3306';

        try {
            $this->dbConnection = new \PDO(
                "mysql:host=$host;port=$port;charset=utf8mb4;dbname=$db",
                $user,
                $pass
            );
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    function getConnection() {
        return $this->dbConnection;
    }
}
