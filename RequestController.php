<?php

include_once('service/PlayService.php');
include_once('service/ReservationService.php');
include_once('DatabaseConnector.php');
require_once('PlaySearchCriteria.php');

class RequestController {

	const STATUS_OK = 'HTTP/1.1 200 OK';
	const STATUS_NOTFOUND = 'HTTP/1.1 404 Not Found';
	const STATUS_BADREQUEST = 'HTTP/1.1 400 Bad Request';
	const STATUS_CREATED = 'HTTP/1.1 201 Created';

	private $requestMethod;
	private $uri;
	private $data;
	
	function __construct($requestMethod, $uri, $data) {
		$this->requestMethod = $requestMethod;
		$this->uri = $uri;
		$this->data = $data;
	}

	function processRequest() {
	    $databaseConnector = new DatabaseConnector(); 
	    $response['status'] = self::STATUS_OK;
	    switch ($this->requestMethod) {
	        case 'GET':
	            $playService = new PlayService($databaseConnector->getConnection());
	            if (!isset($this->uri[2])) {
	                $searchCriteria = new PlaySearchCriteria($this->data);
	                $response['body'] = $playService->getPlays($searchCriteria);
	            } else {
	                if ($this->validateGetSeatsUri()) {
	                    $playId = $this->uri[2];
	                    $performanceId = $this->uri[4];     
	                    try {
	                    	$response['body'] = $playService->getAvaliableSeats($playId, $performanceId);
	                    } catch(Exception $e) {
	                    	$response['status'] = self::STATUS_BADREQUEST;
	                    }
	                } else {
	                    $response['status'] = self::STATUS_BADREQUEST;
	                }
	            }
	            break;
	        case 'POST':
	            if ($this->uri[1] !== 'reservations') {
	                $response['status'] = self::STATUS_BADREQUEST;
	            } else {
	                $performanceId = $this->data['performanceId'];
	                $seatsId = (array) $this->data['seatsId'];
	                $dni = $this->data['dni'];
	                $personName = $this->data['personName'];
	                $reservationService = new ReservationService($databaseConnector->getConnection());
	                $reservationId = $reservationService->makeReservation($performanceId, $seatsId, $dni, $personName);
	                if ($reservationId) {
	                	$response['body'] = json_encode($reservationId);
	                	$response['status'] = self::STATUS_CREATED;
	                } else {
	                	$response['status'] = self::STATUS_BADREQUEST;
	                }
	            }
	            break;
	        default:
	            $response['status'] = self::STATUS_NOTFOUND;
	            break;
	    }
	   
	    if (isset($response['body'])) {
	    	$response['body'] = json_encode($response['body']);
	    }
	    return $response;
	}

	function validateGetSeatsUri() {
	    if (!isset($this->uri[2], $this->uri[3], $this->uri[4])) {
	        return false;
	    }
	    if ($this->uri[3] !== 'performances' || !is_numeric($this->uri[2]) || !is_numeric($this->uri[4])) {
	        return false;
	    }
	    return true;
	}
}