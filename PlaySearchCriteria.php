<?php

class PlaySearchCriteria {
	
	const ORDER_CRITERIA_DATE = 'date';
	const ORDER_CRITERIA_PRICE = 'price';
	const ORDER_ASC = 'ASC';
	const ORDER_DESC = 'DESC';

	private $order;
	private $orderCriteria;
	private $maximunPrice;
	private $minimunPrice;
	private $initialDate;
	private $endDate;

	function __construct(array $values) {
		$this->order = $values['order'] ?? self::ORDER_ASC;
		$this->orderCriteria = $values['orderCriteria'] ?? self::ORDER_CRITERIA_DATE;
		$this->maximunPrice = $values['maximunPrice'] ?? null;
		$this->minimunPrice = $values['minimunPrice'] ?? 0;
		$this->initialDate = $values['initialDate'] ?? new DateTime();
		$this->endDate = $values['endDate'] ?? null;
	}

	function getOrder() {
		return $this->order;
	}

	function getOrderCriteria() {
		return $this->orderCriteria;
	}
}