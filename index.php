<?php

include_once('RequestController.php');

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: OPTIONS,GET,POST,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

$uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$uri = explode('/', $uri);


if ($uri[1] !== 'shows' && $uri[1] !== 'reservations') {
    header(RequestController::STATUS_BADREQUEST);
    exit();
}

$requestMethod = $_SERVER["REQUEST_METHOD"];
switch ($requestMethod) {
    case 'GET':
        $response = (new RequestController($requestMethod, $uri, $_GET))->processRequest();
        break;
    case 'POST':
        $response = (new RequestController($requestMethod, $uri, $_POST))->processRequest();
        break;
    default:
        $response = notFoundResponse();
        break;
}
header($response['status']);
if (isset($response['body'])) {
    echo $response['body'];
}

function notFoundResponse() {
    $response['status'] = RequestController::STATUS_NOTFOUND;
    $response['body'] = null;
    return $response;
}
